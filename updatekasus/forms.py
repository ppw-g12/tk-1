from django import forms
from .models import Relawan

class UpdateKasusForm(forms.ModelForm):
  class Meta:
    model = Relawan
    fields = [
      'positif',
      'kasusAktif',
      'meninggal',
      'sembuh',
      'tanggal',
    ]
    widgets = {
      'positif': forms.TextInput(attrs={'class': 'form-control'}),
      'kasusAktif': forms.TextInput(attrs={'class': 'form-control'}),
      'meninggal': forms.TextInput(attrs={'class': 'form-control'}),
      'sembuh': forms.TextInput(attrs={'class': 'form-control'}),
      'tanggal' : forms.DateField(widget=forms.DateInput(format='%m/%d/%Y', attrs={'class': 'datepicker'}),input_formats=('%m/%d/%Y', )),
    }
