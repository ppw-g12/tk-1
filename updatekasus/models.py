from django.db import models

class UpdateKasus(models.Model):
    positif = models.IntegerField()
    kasusAktif = models.IntegerField()
    meninggal = models.IntegerField()
    sembuh = models.IntegerField()
    tanggal = models.DateField()
    
