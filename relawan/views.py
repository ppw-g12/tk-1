from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, DeleteView
from .forms import RelawanForm
from .models import Relawan
from django.urls import reverse_lazy

class DaftarRelawan(CreateView):
    model = Relawan
    form_class = RelawanForm
    template_name = 'daftar-relawan.html'

class ListRelawan(ListView):
    model = Relawan
    template_name = 'list-relawan.html'

class DetailRelawan(DetailView):
    model = Relawan
    template_name = 'detail-relawan.html'

class HapusRelawan(DeleteView):
    model = Relawan
    template_name = 'hapus-relawan.html'
    success_url = reverse_lazy('relawan:List-relawan')


