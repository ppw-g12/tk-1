from django.db import models
from django.urls import reverse

class Relawan(models.Model):
    nama = models.CharField(max_length=100, default='')
    telp = models.CharField(max_length=20, default='')
    email = models.EmailField(max_length=50, default='')
    swot = models.CharField(max_length=300, default='')
    tertarik = models.CharField(max_length=300, default='')
    alasan = models.CharField(max_length=300, default='')

    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse('relawan:List-relawan')
    
    
    
