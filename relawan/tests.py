from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Relawan

# Create your tests here.
class Testing123(TestCase):
  def setUp(self):
    self.client = Client()
    self.test_subject = Relawan.objects.create(
      nama = "Spongebob",
      telp = "14045",
      email = "krabbypatties@gmail.com",
      swot = "Ceria, tidak serius, Jago Masak, Tinggal di laut",
      tertarik = "Menghabiskan jatah libur untuk menjadi relawan",
      alasan = "Spongebob tidak bisa terkena Corona sehingga percaya diri"
    )

  def test_nama_subject(self):
    self.assertEqual(str(self.test_subject), "Spongebob")

  def test_telp_subject(self):
    self.assertEqual(str(self.test_subject.telp), "14045")

  def test_email_subject(self):
    self.assertEqual(str(self.test_subject.email), "krabbypatties@gmail.com")

  def test_activity_get_absolute_url(self):
    self.assertEqual('/relawan/', self.test_subject.get_absolute_url())

  def test_ada_relawan(self):
    response = Client().get('/relawan/')
    self.assertEquals(response.status_code, 200)

  def test_relawan_ada_judulnya(self):
    response = Client().get('/relawan/')
    html_kembalian = response.content.decode('utf8')
    self.assertIn("RELAWAN", html_kembalian)
    self.assertTemplateUsed(response, 'list-relawan.html')

  def test_ada_daftar_relawan(self):
    response = Client().get('/relawan/Daftar')
    self.assertEquals(response.status_code, 200)

  def test_apakah_di_daftar_ada_form(self):
    response = Client().get('/relawan/Daftar')
    html_kembalian = response.content.decode('utf8')
    self.assertIn('form', html_kembalian)
    self.assertTemplateUsed(response, 'daftar-relawan.html')
  
  def test_ada_hapus_relawan(self):
    response = Client().get('/relawan/Hapus/1')
    self.assertEquals(response.status_code, 200)

  def test_apakah_di_hapus_ada_form(self):
    response = Client().get('/relawan/Hapus/1')
    html_kembalian = response.content.decode('utf8')
    self.assertIn('form', html_kembalian)
    self.assertTemplateUsed(response, 'hapus-relawan.html')

  

  




