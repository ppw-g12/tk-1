from django import forms
from .models import Relawan

class RelawanForm(forms.ModelForm):
  class Meta:
    model = Relawan
    fields = [
      'nama',
      'telp',
      'email',
      'swot',
      'tertarik',
      'alasan',
    ]
    labels = {
            'nama': ('Nama Lengkap'),
            'telp': ('Nomor Handphone'),
            'email': ('Email'),
            'swot' : ('SWOT'),
            'tertarik': ('Tertarik'),
            'alasan': ('Alasan menjadi relawan')
        }
    widgets = {
      'nama': forms.TextInput(attrs={
        'placeholder': "Nama Lengkap Anda",
        'class': 'form-control',
        'col':30
        }),
      'telp': forms.TextInput(attrs={
        'placeholder': "Masukkan Nomor handphone Anda",
        'class': 'form-control',
        'col':30
        }),
      'email': forms.TextInput(attrs={
        'placeholder': "Masukkan email Anda",
        'class': 'form-control',
        'col':30
        }),
      'swot': forms.Textarea(attrs={
        'placeholder': "Strength, Weakness, Opportunity, Threat",
        'class': 'form-control'
        }),
      'tertarik' : forms.Textarea(attrs={
        'placeholder': "What are you Interest in",
        'class': 'form-control',
        'col':120
        }),
      'alasan': forms.Textarea(attrs={
        'placeholder': "Mengapa Anda ingin menjadi Relawan",
        'class': 'form-control',
        'col':120
        })
    }
