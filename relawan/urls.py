from django.urls import path
from . import views
from .views import DaftarRelawan, ListRelawan, HapusRelawan, DetailRelawan

app_name = 'relawan'

urlpatterns = [
  path('', ListRelawan.as_view(), name='List-relawan'),
  path('Daftar', DaftarRelawan.as_view(), name='Daftar-relawan'),
  path('Hapus/<int:pk>', HapusRelawan.as_view(), name='Hapus-relawan'),
  path('Detail/<int:pk>', DetailRelawan.as_view(), name='Detail-relawan')
]