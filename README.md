# TK 1

Tugas Kelompok 1 PPW G12

---

## Nama-nama Anggota Kelompok G12

Berikut adalah nama beserta NPM anggota kelompok G12:
1. Achmad Fikri Adidharma - 1906398692
2. Alfred Prasetio - 1906398894
3. Andhika Rifki Alfariz - 1906398345
4. Az-zahra Syamsa Rahmita Putri -1906399291
5. Maheswara Ananto Argono - 1906398471

## Link Mengenai App Kami

Berikut adalah link Herokuapp untuk app kelompok G12:
http://pac-g12.herokuapp.com/

Berikut adalah link Persona untuk app kelompok G12:
https://docs.google.com/document/d/1YqI1y91h8n4GiJZbVxfAcDAZieCdrZv4WM1OVNHqCvo/edit

Berikut adalah link Wireframe untuk app kelompok G12:
https://www.figma.com/file/iEn0k4RETY9kHMbghfSMQf/WIREFRAME?node-id=0%3A1

Berikut adalah link High Fidelity Mockup untuk app kelompok G12:
https://www.figma.com/file/NZDl39CGA70txLWRK4Ssk1/PROTOTYPE?node-id=0%3A1

## Tentang Aplikasi Kami

Aplikasi yang kami buat ini adalah web yang dapat mencari relawan
untuk membantu menangani Covid-19 di Indonesia. Web ini juga dapat 
membantu meningkatkan kesadaran diri kita untuk tidak keluar disaat 
pandemi ini agar angka Covid bisa menurun. Web ini juga memberikan 
update data angka pasien Covid di daerah kita yang dapat membantu 
menyadarkan diri kita supaya lebih menjaga diri. Aplikasi ini juga 
memberikan info penting mengenai Satgas Covid supaya kita dapat 
membantu menurunkan angka Covid dan memberikan bantuan jika terdapat 
tersangka positif.

## Gambaran Umum Aplikasi

Gambaran umum aplikasi kami adalah sebagai berikut :
1. Homepage
    1. Ringkasan artikel yang berhubungan dengan kasus Covid-19
    2. Pemaparan protokol kesehatan yang harus dijalankan
2. Fitur untuk bergabung menjadi relawan melawan Covid-19
3. Page untuk artikel yang lebih lengkap dari homepage
4. About website kita
5. Hotline untuk Satgas Covid yang bersifat dropdown

## Fitur-Fitur pada Aplikasi
1. Hotline -> form untuk menambahkan hotline : Az-zahra Syamsa Rahmita Putri (1906399291)
2. Artikel -> form untuk menulis artikel : Andhika Rifki Alfariz (1906398345)
3. Artikel -> form untuk membuat komentar artikel : Alfred Prasetio (1906398894)
4. Home (Highlight) -> form untuk update kasus baru : Achmad Fikri Adidharma (1906398692)
5. Form daftar relawan : Maheswara Ananta Argono (1906398471)

## Status Pipelines

Passed



