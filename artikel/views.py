from django.shortcuts import render, redirect
from .models import Artikel, Komentar
from .forms import FormArtikel, FormKomentar
from django.contrib import messages

# Create your views here.

def artikel(request):
    if request.method == "POST":
        formA = FormArtikel(request.POST, request.FILES)
        formB = FormKomentar(request.POST)

        if formA.is_valid():
            formA.save()
            messages.success(request, (f"Artikel {request.POST['judul']} berhasil ditambahkan!"))
            return redirect ('artikel:artikel')
        
        else :
            messages.warning(request, (f"input tidak sesuai!"))
            return redirect('artikel:artikel')
    
    else:
        formA = FormArtikel()
        formB = FormKomentar()
        listArtikel = Artikel.objects.all()
        listKomen = Komentar.objects.all()
        context = {
            'formA' : formA,
            'formB' : formB,
            'listArtikel' : listArtikel,
            'listKomen' : listKomen,
        }
        return render(request, 'artikel/index.html', context)



def detail(request, pk):
    artikel = Artikel.objects.get(id=pk)
    listKomen = Komentar.objects.all()
    context = {
        'artikel' : artikel,
        'listKomen' : listKomen,
    }

    if request.method == "POST" :
        formB = FormKomentar(request.POST)

        if formB.is_valid() :
            formB.save()
            messages.success(request, (f"Komentar berhasil ditambahkan!"))
            return redirect('artikel:detail', pk=artikel.pk)
        
        else :
            messages.warning(request, (f"Input tidak sesuai!"))
            return redirect ('artikel:detail', pk=artikel.pk)
    
    else :
        artikel = Artikel.objects.get(id=pk)
        listKomen = Komentar.objects.filter(artikel = artikel)
        formB = FormKomentar(request.POST)
        context = {
            'artikel' : artikel,
            'formB' : formB,
            'listKomen' : listKomen,
        }
        return render(request, 'artikel/artikel-detail.html', context)

def delete(request, pk):
    artikel = Artikel.objects.get(id=pk)
    artikel.delete()
    # messages.success(request, (f"Artikel {request.GET['artikel']} berhasil dihapus!"))
    messages.success(request, (f"Artikel berhasil dihapus!"))
    return redirect('artikel:artikel')

def deleteKomentar(request,pk):
    komentar = Komentar.objects.get(id=pk)
    Komentar.delete()
    messages.warning(request, (f"{komentar} berhasil dihapus"))
    return redirect('artikel:artikel')