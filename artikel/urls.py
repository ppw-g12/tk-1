from django.urls import path

from . import views

app_name = 'artikel'

urlpatterns = [
    path('', views.artikel, name='artikel'),
    path('detail/<int:pk>/', views.detail, name='detail'),
    path('delete/<str:pk>', views.delete, name='delete'),
    path('delete/komentar/<str:pk>', views.deleteKomentar, name='komentarDelete'),
]