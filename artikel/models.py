from django.db import models
# from django.utils timezone
# from datetime import datetime, date

# Create your models here.


class Artikel(models.Model):
    judul = models.CharField(max_length=50)
    penulis = models.CharField(max_length=50)
    gambar = models.ImageField(upload_to = 'images/')
    konten = models.TextField()
    def __str__(self):
        return self.judul

class Komentar(models.Model):
    nama = models.CharField(max_length=50)
    komen = models.TextField()
    artikel = models.ForeignKey(Artikel, on_delete=models.CASCADE)
    def __str__(self):
        return self.komen
    