from django.test import TestCase, Client
from django.urls import reverse, resolve
from .models import Artikel, Komentar


# Create your tests here.

class TestViews(TestCase):
    def setUp(self):
        self.client = Client()
        self.artikel_url = reverse('artikel')
        self.test_artikel = Artikel.objects.create(
            artikel = 'Bahaya Indomie'
        )
        self.test_komentar = Komentar.objects.create(
            komentar = 'Mantap sehat',
            article = self.test_artikel
        )
        self.test_delete_url = reverse("artikel:artikelDelete", args=[self.test_artikel.id])
        self.test_komentar_delete_url = reverse("home:komentarDelete", args=[self.test_komentar.id])

        def test_artikel_GET(self):
            response = self.client.get(self.artikel_url)
            self.assertEquals(response.status_code, 200)
            self.assertTemplateUsed(response, "artikel/index.html")
        
        def test_artikel_POST(self):
            response = self.client.post(self.artikel_url, {
                "artikel" : "naruto"
            }, follow=True)
            self.assertContains(response, "naruto")

        def test_komentar_POST(self):
            response = self.client.post(self.artikel_url, {
                "komentar" : "keren",
                "article" : "self.test_artikel.id",
            }, follow=True)
            self.assertContains(response, "Hadi")

        def test_notValid_POST(self):
            response = self.client.post(self.artikel_url, {
                "komentar" : "keren",
                "article" : "hihi",
            }, follow=True)
            self.assertContains(response, "Input tidak sesuai")

        def test_komentar_DELETE(self):
            response = self.client.get(self.test_komentar_delete_url, follow=True)
            print(response.content)
            self.assertContains(response, "berhasil dihapus")

        def test_artikel_DELETE(self):
            response = self.client.get(self.test_delete_url, follow=True)
            self.assertContains(response, "berhasil dihapus")