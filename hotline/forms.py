from django.forms import ModelForm
from .models import AddHotline

class FormHotline(ModelForm):
    required_css_class = 'required'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for _, value in self.fields.items():
            value.widget.attrs['placeholder'] = value.help_text

    class Meta:
        model = AddHotline
        fields = '__all__'