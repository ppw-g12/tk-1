#from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from .forms import FormHotline
from .models import AddHotline

# Create your views here.
def html_home_hotline(request):
    listHotline = AddHotline.objects.all().order_by('nama')
    return render(request, 'hotline.html', {'list_hotline' : listHotline})

def html_add_hotline(request):
    listHotline = FormHotline(request.POST or None)
    if (request.method == 'POST' and listHotline.is_valid()):
        listHotline.save()
        return redirect('/hotline/')
    return render(request, 'addhotline.html', {'tabel_form' : listHotline})