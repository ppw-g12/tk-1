from django.test import TestCase, Client
from django.urls import resolve
from .models import AddHotline
from .views import html_add_hotline, html_home_hotline
from .forms import FormHotline

# Create your tests here.
class test_home_hotline(TestCase):
    def test_url_home_exist(self):
        response = Client().get('/hotline/')
        self.assertEqual(response.status_code,200)

    def test_views_home_hotline(self):
        found = resolve('/hotline/')           
        self.assertEqual(found.func, html_home_hotline)
 
class test_add_hotline(TestCase):
    def test_url_add_exist(self):
        response = Client().get('/hotline/addhotline')
        self.assertEqual(response.status_code,200)

    def test_views_add_hotline(self):
        found = resolve('/hotline/addhotline')           
        self.assertEqual(found.func, html_add_hotline)