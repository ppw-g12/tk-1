from django.db import models

# Create your models here.
class AddHotline(models.Model):
    nama = models.CharField('Nama :', help_text='Tuliskan nama organisasi', max_length=50)
    telp = models.CharField('No. Telp :', help_text='Tuliskan no telp organisasi', max_length=50)
    logo = models.CharField('URL logo', help_text='Tuliskan URL', max_length=400)
    alamat = models.CharField('Alamat :', help_text='Tuliskan alamat organisasi', max_length=50)
