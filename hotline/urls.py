from django.contrib import admin
from django.urls import include, path
from . import views

app_name = 'hotline'

urlpatterns = [
    path('', views.html_home_hotline, name='html_home_hotline'),
    path('addhotline', views.html_add_hotline, name='html_add_hotline'),
]