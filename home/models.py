from django.db import models

# Create your models here.
class UpdateKasus(models.Model):
    positif = models.IntegerField()
    kasusAktif = models.IntegerField()
    meninggal = models.IntegerField()
    sembuh = models.IntegerField()
    tanggal = models.DateField()
    def __str__(self):
        return self.positif, self.kasusAktif, self.meninggal, self.sembuh, self.tanggal
