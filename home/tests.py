from django.test import TestCase, Client
from django.urls import reverse
from .models import UpdateKasus

# Create your tests here.
class TestViews(TestCase):
            
    def test_UpdateKasus_GET(self):
        self.client = Client()
        self.updatekasus_url = reverse("home:create")
        response = self.client.get(self.updatekasus_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, "home/daftar-kasus.html")
