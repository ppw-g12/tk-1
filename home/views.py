from django.shortcuts import render, redirect
from .forms import UpdateKasusForm
from .models import UpdateKasus

# Create your views here.

def create(request):
    if request.method == "POST":
        form = UpdateKasusForm(request.POST)
        if form.is_valid():
            kasus = UpdateKasus()
            kasus.positif = form.cleaned_data['positif']
            kasus.kasusAktif = form.cleaned_data['kasusAktif']
            kasus.meninggal = form.cleaned_data['meninggal']
            kasus.sembuh = form.cleaned_data['sembuh']
            kasus.tanggal = form.cleaned_data['tanggal']
            kasus.save()
        return redirect('/')
    else:
        data = UpdateKasus.objects.all()
        form = UpdateKasusForm()
        response = {"kasus":data,'form':form}
        return render(request,'home/daftar-kasus.html', response)

def index(request):
    data = UpdateKasus.objects.all()
    response = {"kasus":data}
    return render(request, 'home/index.html', response)
