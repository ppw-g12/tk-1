from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from django.http import HttpRequest
from django.test import TestCase
from django.test import Client
from aboutus.models import *
from aboutus.forms import *
from aboutus.views import *

# Create your tests here.
class test(TestCase):
    def test_landing_page(self):
        response = Client().get("")
        self.assertEqual(response.status_code, 200)

    def test_landing_page_template(self):
        response = Client().get("/about/")
        self.assertTemplateUsed(response, 'about.html')

    def test_models_create(self):
        Kritik.objects.create(Username="abc", Date="2020-11-21", Pesan="abcd")
        hitung = Kritik.objects.all().count()
        self.assertEqual(hitung, 1)