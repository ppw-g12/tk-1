from django.contrib import admin
from django.urls import include, path
from . import views

app_name = 'aboutus'

urlpatterns = [
    path('', views.about, name="about"),
    path('Kritik/', views.addKritik, name="kritik")
]
