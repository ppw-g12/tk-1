from django.shortcuts import render,redirect
from .models import Kritik
from .forms import KritikForm

# Create your views here.
def about(request):
    kritik = Kritik.objects.all()
    form = KritikForm()
    response = {'kritik' : kritik, 'form' : form}
    return render(request, "about.html", response)

def kritik(request):
    return render(request, "kritik_saran.html")

def addKritik(request):
    if request.method == "POST":
        form = KritikForm(request.POST)
        if form.is_valid():
            kritik = Kritik()
            kritik.Username = form.cleaned_data['Username']
            kritik.Date = form.cleaned_data['Date']
            kritik.Pesan = form.cleaned_data['Pesan']
            kritik.save()
        return redirect('aboutus:kritik')
    else:
        kritik = Kritik.objects.all()
        form = KritikForm()
        response = {'kritik' : kritik, 'form' : form}
        return render(request, 'kritik_saran.html', response)